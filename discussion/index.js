//Query Operators

db.collections.find({query}, {field projection})

db.users.find(
	{"age": { $lt:50 } }

);

db.users.find(
	{"age": { $gte:50 } }

);

db.users.find(
	{"age": { $ne:82 } }

);

db.users.find(
	{"lastName": { $eq: "Hawking", $eq: "Doe" } }

);

db.users.find(
	{"lastName": { $in: ["Hawking", "Doe"]} }
);

db.users.find(
	{
		"courses": { $in: ["HTML", "React"]}
	}
);

//Logical Query Operators
//OR logical operators

db.users.find(
	{ $or: [{"firstName": "Niel"}, {"age": 25}]});

db.users.find(
	{ 
		$or: [
			{"firstName": "Niel"}, 
			{"age": { $gt: 30} }
		]
	}
);

//And logical operator

db.users.find(
    { 
        $and: [ 
            {"age": {$ne: 82}}, 
            {"age": {$ne: 76}} 
       ]}
);

//Field Projection

db.users.find(
	{
		"firstName": "Jane"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact": 1
	}
);

db.users.find(
	{
		"firstName": "Hawking"
	},
	{
		"contact": 0,
		"department": 0
	}
);

db.users.find(
	{
		"firstName": "Neil"
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1,
		"contact": 1
	}
);

db.users.find(
	{
		"firstName": "Bill"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact.phone": 1,
		"_id": 0
	}
);

db.users.find(
	{
		"firstName": "Bill"
	},
	{
            "contact.email": 0
	}
);

db.users.find(
	{
		"firstName": { $regex: "N"}
	}
);

db.users.find(
	{
		"firstName": { $regex: "j"}
	}
);

db.users.find(
	{
		"firstName": { $regex: "n", $options: "i"}
	}
);